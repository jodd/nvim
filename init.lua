require "jbm.settings"
require "jbm.options"
require "jbm.plugins"
require "jbm.autocmd"
require "jbm.keybinds"
--
-- ---Pretty print lua table
-- function _G.dump(...)
--     local objects = vim.tbl_map(vim.inspect, { ... })
--     print(unpack(objects))
-- end
