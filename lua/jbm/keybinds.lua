local function map(m, k, v)
    vim.keymap.set(m, k, v, { silent = true })
end

map('n', '*', '*N')                               -- Fix * (Keep the cursor position, don't move to next match)
map('n', 'n', 'nzz')                              -- Fix n and N. Keeping cursor in center
map('n', 'N', 'Nzz')

-- buffers
-- map('n', '<leader>bb', ':bnext<CR>')                --move to next buffer
-- map('n', '<leader>BB', ':bprevious<CR>')            --move to previous buffer
-- map('n', '<leader>BU', ':bunload<CR>')              --unload buffer
map('n', '<tab>', ':bnext<CR>')                      -- Move to the next/previous buffer
map('n', '<S-tab>', ':bprevious<CR>')
-- map('n', '<leader>tn', ':tabnew<CR>')                -- New tab
-- map('n', '<leader>tp', ':tabnew<C-r*<CR>')

map('v', '<', '<gv')                              -- visual tabindent
map('v', '>', '>gv')
--
map('i', 'jj', '<esc>')
map('i', 'JJ', '<esc>la, ')
map('v', 'jj', '<esc>')
-- map('n', '<leader>cs', ':colorscheme ')           -- colorschemes
map('n', '<leader>fm', ':Vifm<CR>')               -- vifm File Manager
-- split stuff
map('n', '<A-h>', '<C-w>h')                       -- navigation
map('n', '<A-j>', '<C-w>j')
map('n', '<A-k>', '<C-w>k')
map('n', '<A-l>', '<C-w>l')
map('n', '<A-Left>', ':vertical resize +3<CR>')   -- resize
map('n', '<A-Right>', ':vertical resize +3<CR>')
map('n', '<A-Up>', ':resize +3<CR>')
map('n', '<A-Down>', ':resize +3<CR>')
map('n', '<leader>h', ':split<CR>')               -- split direction
map('n', '<leader>v', ':vsplit<CR>')
map('n', '<leader>d', ':only<CR>')                -- delete split
--/split
-- colorizer
map('n', '<leader>c', ':ColorizerToggle<CR>')         --
