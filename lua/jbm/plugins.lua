-- Automatically run :PackerCompile whenever plugins.lua is updated with an autocommand:
vim.api.nvim_create_autocmd('BufWritePost', {
    group = vim.api.nvim_create_augroup('PACKER', { clear = true }),
    pattern = 'plugins.lua',
    command = 'source <afile> | PackerCompile',
})

return require('packer').startup({
    function(use)
        use('wbthomason/packer.nvim')
        -- Required plugins --
        use('nvim-lua/plenary.nvim')
        use({
            'kyazdani42/nvim-web-devicons',
            config = function()
                require('nvim-web-devicons').setup()
            end,
        })
        -- use('nvim-treesitter/nvim-treesitter')
        -- use('nvim-telescope/telescope.nvim')
        ----------------------------------------
        use 'norcalli/nvim-colorizer.lua'                           -- https://github.com/norcalli/nvim-colorizer.lua
        use "lunarvim/colorschemes"                                 -- https://github.com/LunarVim/Colorschemes
        use "vifm/vifm.vim"                                         -- vifm
        -- use 'scrooloose/nerdtree'                                 --fromt dt
        -- use 'tiagofumo/vim-nerdtree-syntax-highlight'             --fromt dt
        -- use 'ryanoasis/vim-devicons'                              --fromt dt
        use({
          'vimwiki/vimwiki',                        --jbm virker
          config = function()
          vim.g.vimwiki_list = {
            {
              path = '/home/jan/vimwiki/',
              syntax = 'markdown',
              ext = '.md',
            }
          }
          end,
        })
        use({
            {
              'nvim-lualine/lualine.nvim',                    -- https://github.com/nvim-lualine/lualine.nvim
                -- event = 'BufEnter',
              config = function()
                require('jbm.plugins.lualine')
              end,
            },
            {
               'j-hui/fidget.nvim',                           -- IDK, don't really know about the function of this
                after = 'lualine.nvim',                       -- https://github.com/j-hui/fidget.nvim
                config = function()
                  require('fidget').setup()
                end,
              }
        })
        use({
            'numToStr/Comment.nvim',
            config = function()
                require('Comment').setup()
            end,
        })
        -- cmp-nvim-lsp
        -- lsp
        -- use({
        --     'neovim/nvim-lspconfig',
        --     config = function()
        --         require('jbm.plugins.lsp.servers')
        --     end,
        --     requires = {
        --         {
        --             -- WARN: Unfortunately we won't be able to lazy load this
        --             'hrsh7th/cmp-nvim-lsp',
        --         },
        --     },
        -- })
    end,
})
---                                                     
--                                                      
-- from DT
--   -- A better status line
--   use {
--     'nvim-lualine/lualine.nvim',
--     requires = { 'kyazdani42/nvim-web-devicons', opt = true }
--   }
--
--   -- File management --
--   use 'vifm/vifm.vim'
--   use 'scrooloose/nerdtree'
--   use 'tiagofumo/vim-nerdtree-syntax-highlight'
--   use 'ryanoasis/vim-devicons'
--
--   -- Productivity --
--   use 'vimwiki/vimwiki'
--   use 'jreybert/vimagit'
--
--   -- Tim Pope Plugins --
--   use 'tpope/vim-surround'
--
--   -- Syntax Highlighting and Colors --
--   use 'PotatoesMaster/i3-vim-syntax'
--   use 'kovetskiy/sxhkd-vim'
--   use 'vim-python/python-syntax'
--   use 'ap/vim-css-color'
--
--   -- Junegunn Choi Plugins --
--   use 'junegunn/goyo.vim'
--   use 'junegunn/limelight.vim'
--   use 'junegunn/vim-emoji'
--
--   -- Colorschemes
--   use 'RRethy/nvim-base16'
--   use 'kyazdani42/nvim-palenight.lua'
--
--   -- Other stuff
--   use 'frazrepo/vim-rainbow'
-- end)
-- /DT
--
-- "vimwiki makes and reads .md instead of .wiki
-- let g:vimwiki_list = [{'path': '~/Public/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]

        -- use({
        --     'numToStr/Buffers.nvim',
        --     event = 'BufRead',
        --     config = function()
        --         require('jbm.plugins.buffers')
        --     end,
        -- })
